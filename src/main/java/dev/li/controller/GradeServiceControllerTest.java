package dev.li.controller;

import dev.li.dao.GradeDao;
import dev.li.domain.GradeReport;
import dev.li.service.GradeService;
import org.junit.Test;

import java.util.List;

public class GradeServiceControllerTest {

private GradeService gradeService = new GradeService();

@Test
    public void findAll(){
    List<GradeReport> list = gradeService.getAll();
    for (GradeReport grade : list){
        System.out.println(grade);
    }
}

@Test
    public void insertGrade(){
    GradeReport gradeReport = new GradeReport(80,90);
    gradeService.insertGrade(gradeReport);
    GradeReport gradeReport2 = new GradeReport(90,100);
    gradeService.insertGrade(gradeReport2);
}

@Test
    public void deleteGrade(){
    gradeService.delete(2);
}
}
