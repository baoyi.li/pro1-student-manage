package dev.li.controller;

import dev.li.domain.Student;
import dev.li.service.StudentService;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.NotFoundResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import io.javalin.http.Context;

import java.util.List;

public class StudentController {
    private StudentService studentService =new StudentService();
    private final Logger logger = LoggerFactory.getLogger(StudentController.class);

    public void handleGetStudentsRequest(Context ctx){
        logger.info("getting all students");
        List<Student> list = studentService.getAll();
        ctx.json(list);
    }

    public void handleGetStudentBySidRequest(Context ctx) {
        String idString = ctx.pathParam("id");
        if (idString.matches("^\\d+$")) {
            int idInput = Integer.parseInt(idString);
            Student stu = studentService.findById(idInput);
            if (stu == null) {
                ctx.status(404);
                logger.warn("no student present with id: " + idInput);
                throw new NotFoundResponse("No Student found with provided ID: " + idInput);
            } else {
                logger.info("getting student with id: " + idInput);
                ctx.json(stu);
            }
        } else {
            throw new BadRequestResponse("input \"" + idString + "\" cannot be parsed to an int");
        }
    }

    public void handlePostNewStudent(Context ctx) {
        Student stu = ctx.bodyAsClass(Student.class);
        logger.info("adding new Student" + stu);
        studentService.insert(stu);
        ctx.status(201);
    }

    public void handleDeleteById(Context ctx) {
        String idString = ctx.pathParam("id");
        if(idString.matches("^\\d+$")){
            int idInput = Integer.parseInt(idString);
            logger.info("deleting student in the record with id: "+idInput);
            studentService.delete(idInput);
        } else {
            throw new BadRequestResponse("input \""+idString+"\" cannot be parsed to an int");
        }

    }

    public void handleUpdateByID(Context ctx) {
        String idString = ctx.pathParam("id");
        Student stu = ctx.bodyAsClass(Student.class);

        if(idString.matches("^\\d+$")){
            int idInput = Integer.parseInt(idString);
            logger.info("updating student in the record with id: "+idInput);
            studentService.update(stu);
        } else {
            throw new BadRequestResponse("input \""+idString+"\" cannot be parsed to an int");
        }
    }
}
