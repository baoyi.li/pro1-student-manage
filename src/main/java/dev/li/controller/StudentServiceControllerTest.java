package dev.li.controller;

import dev.li.domain.Student;
import dev.li.service.StudentService;
import org.junit.Test;

import java.util.List;

public class StudentServiceControllerTest {
    private StudentService studentService = new StudentService();

@Test
public void findAll(){
    List<Student> list = studentService.getAll();
    for (Student stu : list){
        System.out.println(stu);
    }
}
    @Test
    public void insertStudent(){
        Student stu = new Student("Sara McMurray", "413-747-2019", "SaraM@student.edu");
        Student stu1 = new Student("Carl Strong", "707-578-2081", "CarlS@student.edu");
        Student stu2 = new Student("Clara Soto", "660-684-9945", "ClaraS@student.edu");
        Student stu3 = new Student("Sharon Phillips", "218-454-2396", "SharonP@student.edu");
        Student stu4 = new Student("Steve Lawrence", "402-423-5272", "SteveL@student.edu");
        Student stu5 = new Student("Joyce Gonzalez", "907-734-3460", "JoyceG@student.edu");
        studentService.insert(stu);
        studentService.insert(stu1);
        studentService.insert(stu2);
        studentService.insert(stu3);
        studentService.insert(stu4);
        studentService.insert(stu5);
    }

    @Test
    public void updateStudent(){
        Student stu = new Student(2,"Jollin", "206-000-1111", "Jonllin@student.edu");
        studentService.update(stu);
    }

@Test
public void deleteStudent(){ studentService.delete(2);}




}