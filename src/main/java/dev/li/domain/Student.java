package dev.li.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table( name = "Student" )
@JsonIgnoreProperties("hibernateLazyInitializer")
public class Student implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private Integer sid;
    private String name;
    private String phone;
    private String email;

    public Student() {
    }

    public Student(Integer sid, String name, String phone, String email) {
        this.sid = sid;
        this.name = name;
        this.phone = phone;
        this.email = email;
    }

    public Student(String name, String phone, String email) {
        this.sid = sid;
        this.name = name;
        this.phone = phone;
        this.email = email;
    }

    public int getSid() {
        return sid;
    }

    public void setSid(Integer sid) {
        this.sid = sid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Student{" +
                "sid=" + sid +
                ", name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
