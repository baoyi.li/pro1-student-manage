package dev.li.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "grade")
public class GradeReport implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int gradeID;
    private int midterm;
    private int finalScore;

    public GradeReport() {
    }

    public GradeReport(int gradeID, int midterm, int finalScore) {
        this.gradeID = gradeID;
        this.midterm = midterm;
        this.finalScore = finalScore;
    }

    public GradeReport(int midterm, int finalScore) {
        this.midterm = midterm;
        this.finalScore = finalScore;
    }

    public int getGradeID() {
        return gradeID;
    }

    public void setGradeID(int gradeID) {
        this.gradeID = gradeID;
    }

    public int getMidterm() {
        return midterm;
    }

    public void setMidterm(int midterm) {
        this.midterm = midterm;
    }

    public int getFinalScore() {
        return finalScore;
    }

    public void setFinalScore(int finalScore) {
        this.finalScore = finalScore;
    }

    @Override
    public String toString() {
        return "GradeReport{" +
                "gradeID=" + gradeID +
                ", midterm=" + midterm +
                ", finalScore=" + finalScore +
                '}';
    }
}
