package dev.li.service;

import dev.li.dao.StudentDao;
import dev.li.dao.StudentDaoImpl;
import dev.li.domain.Student;

import java.util.List;

public class StudentService {
    private final StudentDao studentDao = new StudentDaoImpl();

    public List<Student> getAll() {return studentDao.findAll();}

    public Student findById(Integer sid){return studentDao.findById(sid);}

    public Student insert(Student stu){return studentDao.insert(stu);}

    public Student update(Student stu){return studentDao.update(stu);}

    public void delete(Integer sid){
        studentDao.deleteById(sid);
    }

}
