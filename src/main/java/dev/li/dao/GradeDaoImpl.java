package dev.li.dao;



import dev.li.domain.GradeReport;
import dev.li.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.List;

public class GradeDaoImpl implements GradeDao {
    Logger logger = LoggerFactory.getLogger(GradeDaoImpl.class);
    @Override
    public List<GradeReport> getAllGrades() {
        try (Session s = HibernateUtil.getSession()){
            logger.info("getting all grades");
            return s.createQuery("from GradeReport", GradeReport.class).list();
        }
    }

    @Override
    public GradeReport findById(Integer gradeId) {
        try(Session s = HibernateUtil.getSession()){
            GradeReport gradeReport = s.get(GradeReport.class, gradeId);
            return gradeReport;
        }

    }

    @Override
    public GradeReport insertGrade(GradeReport gradeReport) {
        try(Session s = HibernateUtil.getSession()){
            Transaction tx = s.beginTransaction();
            int id = (int)s.save(gradeReport);
            gradeReport.setGradeID(id);
            logger.info("added new grade report with id" + id);
            tx.commit();
            return gradeReport;
        }

    }

    @Override
    public int updateGrade(GradeReport gradeReport) {
        return 0;
    }

    @Override
    public void deleteById(Integer gradeId) {
        try(Session s = HibernateUtil.getSession()){
            Transaction tx = s.beginTransaction();
            GradeReport gradeReport = (GradeReport)s.load(GradeReport.class, gradeId);
            s.delete(gradeReport);
            logger.info("delete grade with id" + gradeId);
            tx.commit();
        }
    }
}
