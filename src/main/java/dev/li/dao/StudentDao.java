package dev.li.dao;

import dev.li.domain.Student;

import java.util.List;

public interface StudentDao {
    public abstract List<Student> findAll();

    public abstract Student findById(Integer id);

    public abstract Student insert(Student stu);

    public abstract Student update(Student stu);

    public abstract void deleteById(Integer id);
}
