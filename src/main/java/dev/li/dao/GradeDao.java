package dev.li.dao;

import dev.li.domain.GradeReport;

import java.util.List;

public interface GradeDao {
    List<GradeReport> getAllGrades();
    GradeReport findById(Integer gradeId);
    GradeReport insertGrade(GradeReport gradeReport);
    int updateGrade(GradeReport gradeReport);
    void deleteById(Integer gradeId);
}
