package dev.li.dao;

import dev.li.domain.Student;
import dev.li.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class StudentDaoImpl implements StudentDao{
    Logger logger = LoggerFactory.getLogger(StudentDaoImpl.class);
    @Override
    public List<Student> findAll() {
        try (Session s = HibernateUtil.getSession()){
            logger.info("getting all grades");
            return s.createQuery("from Student", Student.class).list();
        }
    }

    @Override
    public Student findById(Integer sid) {
        try(Session s = HibernateUtil.getSession()){
            Student student = s.get(Student.class, sid);
            return student;
        }
    }

    @Override
    public Student insert(Student stu) {
        try(Session s = HibernateUtil.getSession()){
            Transaction tx = s.beginTransaction();
            int sid = (int)s.save(stu);
            stu.setSid(sid);
            logger.info("add new student with id" + sid);
            tx.commit();
            return stu;
        }
    }

    @Override
    public Student update(Student stu) {
        try(Session s = HibernateUtil.getSession()){
            Transaction tx = s.beginTransaction();
            s.update(stu);
            logger.info("update student with id" + stu.getSid());
            tx.commit();
        }
        return null;
    }

    @Override
    public void deleteById(Integer sid) {
        try(Session s = HibernateUtil.getSession()){
            Transaction tx = s.beginTransaction();
            Student stu = (Student) s.load(Student.class, sid);
            s.delete(stu);
            logger.info("delete student with id" + sid);
            tx.commit();
        }
    }
}
