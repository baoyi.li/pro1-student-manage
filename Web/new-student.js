document.getElementById("new-student-form").addEventListener("submit", addNewStudent);

function addNewStudent(e){
    e.preventDefault();
    const studentName = document.getElementById("studentName").value;
    const studentPhone = document.getElementById("studentPhone").value;
    const studentEmail = document.getElementById("studentEmail").value;
    const newStudent = {"name": studentName, "phone": studentPhone, "email": studentEmail};
    // javascript object (newItem) -> json -> java object (MarketItem.class)
    ajaxCreateStudent(newStudent, indicateSuccess, indicateFailure);
}

function indicateSuccess(){
    const message = document.getElementById("create-msg"); 
    message.hidden = false;
    message.innerText = "New student successfully created";
}

function indicateFailure(){
    const message = document.getElementById("create-msg"); 
    message.hidden = false;
    message.innerText = "There was an issue creating your new student";
}
