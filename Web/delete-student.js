document.getElementById("delete-student-form").addEventListener("submit", deleteStudent);

function deleteStudent(e){
    e.preventDefault();
    const sid = document.getElementById("studentID").value;
    console.log(sid);

    // javascript object (newItem) -> json -> java object (MarketItem.class)
    ajaxDeleteStudent(sid, indicateSuccess, indicateFailure);
}

function indicateSuccess(){
    const message = document.getElementById("delete-msg"); 
    message.hidden = false;
    message.innerText = "student successfully delete";
}

function indicateFailure(){
    const message = document.getElementById("delete-msg"); 
    message.hidden = false;
    message.innerText = "There was an issue deleting student";
}



